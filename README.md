# prova_4all

Prova técnica de automação de teste para o processo seletivo na 4all.

## Rodando na máquina localmente!

Para rodar o projeto localmente com tranquilidade, você precisa ter instalado:

- SO linux
- IDE da sua preferência
- Java 1.8+
- Maven 3.3+

Para rodar os testes, basta executar os seguintes comandos no terminal:


```sh
$ mvn clean 
```
```sh
$ mvn package
```

Após a execução, é possível verificar o report dos dois cenários executados na pasta 'reports'

![description](report.jpeg)


