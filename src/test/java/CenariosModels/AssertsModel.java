package CenariosModels;

import Pages.AssertsPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class AssertsModel {

    private AssertsPage validacoes;

    public AssertsModel(WebDriver driver) {
        this.validacoes = new AssertsPage(driver);
    }

    public boolean validaMsgCompraSucesso(){
        return validacoes.getMensagemSucessoLabel().getText().contains("Pedido realizado com sucesso!");
    }

    public void fecharPopUpMsgSucesso(){
        validacoes.getFecharPopUpMensagemButton().click();
    }

    public void verificaQtdRisolesNoCarrinho(){

        try {
            String risoles = validacoes.getQtdRisolesLabel().getText();
            Assert.assertEquals("5", risoles);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
