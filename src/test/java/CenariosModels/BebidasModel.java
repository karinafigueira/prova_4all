package CenariosModels;

import Pages.BebidasPage;

import org.openqa.selenium.WebDriver;

public class BebidasModel {

    private BebidasPage bebidasAppObject;

    public BebidasModel(WebDriver driver) {
        this.bebidasAppObject = new BebidasPage(driver);
    }

    public void adicionaBebidasAoCarrinho() {
        bebidasAppObject.getAdicionaCocaColaAoCarrinhoButton().click();
        bebidasAppObject.getAdicionaFantaUvaAoCarrinhoButton().click();
        bebidasAppObject.getAdicionaAguaMineralAoCarrinhoButton().click();
    }

}
