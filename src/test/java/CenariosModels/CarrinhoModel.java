package CenariosModels;

import Pages.CarrinhoPage;
import org.openqa.selenium.WebDriver;

public class CarrinhoModel {

    private CarrinhoPage cartPage;

    public CarrinhoModel(WebDriver driver) {
        this.cartPage = new CarrinhoPage(driver);
    }

    public void aumentaQtdBrigadeirosNoCarrinho() {
        cartPage.getAumentaQtdBrigadeiroNoCarrinhoButton().click();
    }

    public void finalizarCompras() {
        cartPage.getFinalizarCompraButton().click();
    }

    public void aumentaQtdRissolesNoCarrinho() {
        cartPage.getAumentaQtdRissolesNoCarrinhoButton().click();
    }

    public void diminuiQtdRissolesNoCarrinho() {
        cartPage.getDiminuiQtdRissolesNoCarrinho().click();
    }

}
