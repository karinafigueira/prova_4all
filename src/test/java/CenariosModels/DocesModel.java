package CenariosModels;

import org.openqa.selenium.WebDriver;
import Pages.DocesPage;

public class DocesModel {

    private DocesPage docesPage;

    public DocesModel(WebDriver driver){
        this.docesPage = new DocesPage(driver);
    }

    public void adicionaDocesNoCarrinho() {
        docesPage.getAdicionaBrigadeiroAoCarrinhoButton().click();
        docesPage.getAdicionaAlfajorAoCarrinhoButton().click();
    }

}
