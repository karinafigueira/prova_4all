package CenariosModels;

import org.openqa.selenium.WebDriver;
import Pages.HomePage;

public class HomeModel {

    private final HomePage home;

    public HomeModel(WebDriver driver){
        this.home = new HomePage(driver);
    }

    public void acessaCategoriaDoces() {
        home.getAbreCategoriasButton().click();
        home.getCategoriaDocesButton().click();
    }

    public void acessaCarrinhoCompras() {
        home.getCarrinhoButton().click();
    }
    public void acessaTodasCategorias() {
        home.getAbreCategoriasButton().click();
        home.getCategoriaTodosButton().click();
    }

    public void acessaCategoriaBebidas() {
        home.getAbreCategoriasButton().click();
        home.getCategoriaBebidasButton().click();
    }


}
