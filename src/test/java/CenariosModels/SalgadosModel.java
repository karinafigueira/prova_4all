package CenariosModels;

import Pages.HomePage;
import org.openqa.selenium.WebDriver;
import Pages.SalgadosPage;

public class SalgadosModel {

    private SalgadosPage salgados;

    public SalgadosModel(WebDriver driver){
        this.salgados = new SalgadosPage(driver);
    }

        public void adicionaRissolesAoCarrinho() { salgados.getAdicionaRissolesAoCarrinho().click(); }
}
