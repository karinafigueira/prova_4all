package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SalgadosPage {

    public WebDriver driver;

    public SalgadosPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getAdicionaRissolesAoCarrinho() {
        return this.driver.findElement(By.id("add-product-3-btn"));
    }
}
