package Testes;

import Util.Report;
import Util.ScreenShot;
import com.aventstack.extentreports.Status;
import org.junit.Test;
import Util.TestUtils;


public class Testes extends TestUtils{

    @Test
        public void Desafio1() {
            Report.startTest("Desafio1");
            home.acessaCategoriaDoces();
            doces.adicionaDocesNoCarrinho();
            Report.log(Status.INFO, "Doces adicionados ao carrinho", ScreenShot.capture(driver));
            home.acessaTodasCategorias();
            Report.log(Status.INFO, "Acesso todas as categorias ok", ScreenShot.capture(driver));
            home.acessaCarrinhoCompras();
            for(int i = 0; i < 3; i++) {
                carrinhoCompras.aumentaQtdBrigadeirosNoCarrinho();
            }
            Report.log(Status.INFO, "Adicionados mais 3 brigadeiros.", ScreenShot.capture(driver));
            carrinhoCompras.finalizarCompras();
            Report.log(Status.INFO, "Compra finalizada", ScreenShot.capture(driver));
            validacoes.validaMsgCompraSucesso();
            Report.log(Status.INFO, "Mensagem de sucesso", ScreenShot.capture(driver));
            validacoes.fecharPopUpMsgSucesso();

        }

    @Test
    public void Desafio2() {
        Report.startTest("Desafio2");
        home.acessaCategoriaBebidas();
        bebidas.adicionaBebidasAoCarrinho();
        Report.log(Status.INFO, "Adiciona as bebidas ao carrinho", ScreenShot.capture(driver));
        home.acessaTodasCategorias();
        salgados.adicionaRissolesAoCarrinho();
        Report.log(Status.INFO, "Risole adicionado ao carrinho", ScreenShot.capture(driver));
        home.acessaCarrinhoCompras();
        Report.log(Status.INFO, "Acesso ao carrinho OK", ScreenShot.capture(driver));
        for(int i = 0; i < 8; i++) {
            carrinhoCompras.aumentaQtdRissolesNoCarrinho();
        }

        Report.log(Status.INFO, "Adicionados mais 7 risoles.", ScreenShot.capture(driver));
        for(int i = 0; i < 4; i++) {
            carrinhoCompras.diminuiQtdRissolesNoCarrinho();
        }
        Report.log(Status.INFO, "Alterada quantidade de risoles para 5", ScreenShot.capture(driver));
        validacoes.verificaQtdRisolesNoCarrinho();

        carrinhoCompras.finalizarCompras();
        Report.log(Status.INFO, "Compra finalizada", ScreenShot.capture(driver));
        validacoes.validaMsgCompraSucesso();
        validacoes.fecharPopUpMsgSucesso();

    }

}

