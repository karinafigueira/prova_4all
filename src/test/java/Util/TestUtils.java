package Util;

import CenariosModels.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class TestUtils {

    public WebDriver driver;
    public HomeModel home;
    public DocesModel doces;
    public CarrinhoModel carrinhoCompras;
    public AssertsModel validacoes;
    public BebidasModel bebidas;
    public SalgadosModel salgados;

    @Before
    public void setUp() {

        System.setProperty("webdriver.chrome.driver", "src/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://shopcart-challenge.4all.com/");
        driver.manage().window().maximize();
        home = new HomeModel(driver);
        carrinhoCompras = new CarrinhoModel(driver);
        doces = new DocesModel(driver);
        validacoes = new AssertsModel(driver);
        bebidas = new BebidasModel(driver);
        salgados = new SalgadosModel(driver);

    }
    @After
    public void tearDown() {
        driver.quit();
        Report.close();
    }

}
